package com.zumepizza.interview.data.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zumepizza.interview.data.model.MenuBadge;

import java.lang.reflect.Type;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class MenuBadgeConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static MenuBadge stringToSomeObject(String data) {

        Type listType = new TypeToken<MenuBadge>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(MenuBadge someObjects) {
        return gson.toJson(someObjects);
    }

}
