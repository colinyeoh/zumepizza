package com.zumepizza.interview.data.dao;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Update;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by ColinYeoh on 5/12/18.
 */

public abstract class BaseDao<T> {

    @Insert(onConflict = REPLACE)
    public abstract Long insert(T type);

    @Update
    public abstract int update(T type);

    @Delete
    public abstract int delete(T type);
}
