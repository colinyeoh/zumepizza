package com.zumepizza.interview.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.zumepizza.interview.data.dao.CartItemDao;
import com.zumepizza.interview.data.dao.PizzaModelDao;
import com.zumepizza.interview.data.model.CartItem;
import com.zumepizza.interview.data.model.PizzaModel;

/**
 * Created by ColinYeoh on 5/11/18.
 */
@Database(entities = {
        CartItem.class,
        PizzaModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;
    private static String DB_NAME = "zume_pizza_db";

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DB_NAME)
                            .build();
        }
        return INSTANCE;
    }

    public abstract CartItemDao cartItemDao();

    public abstract PizzaModelDao pizzaModelDao();

}
