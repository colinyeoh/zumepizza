package com.zumepizza.interview.data.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by ColinYeoh on 5/12/18.
 */

public class ObjectTypeConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static List<Object> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Object>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<Object> someObjects) {
        return gson.toJson(someObjects);
    }
}
