package com.zumepizza.interview.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.zumepizza.interview.data.typeconverter.DateConverter;
import com.zumepizza.interview.data.typeconverter.PizzaModelTypeConverter;

import java.util.Date;

/**
 * Created by ColinYeoh on 5/11/18.
 */

@Entity(tableName = "CartItem")
public class CartItem {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private Integer id;

    @ColumnInfo(name = "pizza_id")
    private Integer pizzaId;

    @SerializedName("pizza")
    @TypeConverters(PizzaModelTypeConverter.class)
    private PizzaModel pizza;

    @SerializedName("creationDate")
    @TypeConverters(DateConverter.class)
    private Date creationDate;

    @SerializedName("count")
    private int count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(Integer pizzaId) {
        this.pizzaId = pizzaId;
    }

    public PizzaModel getPizza() {
        return pizza;
    }

    public void setPizza(PizzaModel pizza) {
        this.pizza = pizza;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
