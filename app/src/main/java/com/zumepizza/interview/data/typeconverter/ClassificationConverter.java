package com.zumepizza.interview.data.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zumepizza.interview.data.model.Classifications;

import java.lang.reflect.Type;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class ClassificationConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static Classifications stringToSomeObject(String data) {

        Type listType = new TypeToken<Classifications>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(Classifications someObjects) {
        return gson.toJson(someObjects);
    }

}
