package com.zumepizza.interview.data.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zumepizza.interview.data.model.MenuAsset;

import java.lang.reflect.Type;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class MenuAssetConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static MenuAsset stringToSomeObject(String data) {

        Type listType = new TypeToken<MenuAsset>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(MenuAsset someObjects) {
        return gson.toJson(someObjects);
    }

}
