package com.zumepizza.interview.data.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.zumepizza.interview.data.typeconverter.DateConverter;
import com.zumepizza.interview.data.model.PizzaModel;

import java.util.List;


/**
 * Created by ColinYeoh on 5/11/18.
 */

@Dao
@TypeConverters({DateConverter.class})
public abstract class PizzaModelDao extends BaseDao<PizzaModel>{

    @Query("select * from PizzaModel")
    public abstract LiveData<List<PizzaModel>> getAllPizzaModel();

}
