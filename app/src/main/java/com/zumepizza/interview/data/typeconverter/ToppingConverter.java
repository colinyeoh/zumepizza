package com.zumepizza.interview.data.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zumepizza.interview.data.model.Topping;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class ToppingConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static List<Topping> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Topping>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<Topping> someObjects) {
        return gson.toJson(someObjects);
    }

}
