package com.zumepizza.interview.data.typeconverter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zumepizza.interview.data.model.Asset;

import java.lang.reflect.Type;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class AssetConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static Asset stringToSomeObject(String data) {

        Type listType = new TypeToken<Asset>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(Asset someObjects) {
        return gson.toJson(someObjects);
    }

}
