package com.zumepizza.interview.data.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.TypeConverters;

import com.zumepizza.interview.data.model.CartItem;
import com.zumepizza.interview.data.typeconverter.DateConverter;
import com.zumepizza.interview.data.typeconverter.PizzaModelTypeConverter;

import java.util.List;

/**
 * Created by ColinYeoh on 5/11/18.
 */

@Dao
@TypeConverters({DateConverter.class, PizzaModelTypeConverter.class})
public abstract class CartItemDao extends BaseDao<CartItem> {

    @Query("select * from CartItem")
    public abstract LiveData<List<CartItem>> getAllCartItems();

    @Query("update CartItem set count = count + 1 where id = :id")
    public abstract int incrementCartItemCount(int id);

    @Query("select * from CartItem where pizza_id = :id")
    public abstract CartItem getCartItemByPizzaId(int id);

    @Query("select * from CartItem where id = :id")
    public abstract CartItem getCartItemById(int id);

    @Query("select count(*) from CartItem")
    public abstract LiveData<Integer> getCount();

    @Transaction
    public long addToCart(CartItem cartItem){
        CartItem item = getCartItemByPizzaId(cartItem.getPizza().getId());
        long result = 0;
        if (item != null){
            result = Long.valueOf(incrementCartItemCount(item.getId()));
        } else {
            result = insert(cartItem);
        }
        return result;
    }

    @Transaction
    public int decrementCartItemCount(int cartItemId){
        CartItem item = getCartItemById(cartItemId);
        if (item == null){
            return -1;
        } else {

            long result = 0;

            if (item.getCount() > 1){

                // decrease the count
                item.setCount(item.getCount() - 1);
                result = update(item);

            } else {

                // Remove from list
                result = delete(item);
            }

            return result <= 0 ? -1 : 0;
        }
    }
}
