package com.zumepizza.interview.view.checkout.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.zumepizza.interview.data.AppDatabase;
import com.zumepizza.interview.data.model.CartItem;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ColinYeoh on 5/12/18.
 */

public class CheckoutViewModel extends AndroidViewModel {

    private AppDatabase db;
    private LiveData<List<CartItem>> cartItems;

    public CheckoutViewModel(@NonNull Application application) {
        super(application);

        db = AppDatabase.getDatabase(this.getApplication());

        cartItems = db.cartItemDao().getAllCartItems();

    }

    public LiveData<List<CartItem>> getCartItems(){
        return this.cartItems;
    }

    public void increment(final int cartItemId) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.cartItemDao().incrementCartItemCount(cartItemId);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    public void decrement(final int cartItemId) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.cartItemDao().decrementCartItemCount(cartItemId);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    public void delete(final CartItem cartItem){
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.cartItemDao().delete(cartItem);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

}
