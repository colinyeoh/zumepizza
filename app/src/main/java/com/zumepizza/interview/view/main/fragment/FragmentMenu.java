package com.zumepizza.interview.view.main.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zumepizza.interview.R;
import com.zumepizza.interview.custom.CustomSnackbar;
import com.zumepizza.interview.data.model.PizzaModel;
import com.zumepizza.interview.view.checkout.fragment.CheckoutFragment;
import com.zumepizza.interview.view.main.adapter.MenuRecyclerViewAdapter;
import com.zumepizza.interview.view.main.viewmodel.MenuViewModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ColinYeoh on 5/11/18.
 */

public class FragmentMenu extends Fragment implements MenuRecyclerViewAdapter.OnAddToCartClickListener {

    private MenuRecyclerViewAdapter menuRecyclerViewAdapter;
    private MenuViewModel menuViewModel;
    private RecyclerView recyclerView;
    private int cartCount = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_main, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(final @NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setRetainInstance(true);

        recyclerView = view.findViewById(R.id.menu_recyclerview);


        // Setup list adapter
        menuRecyclerViewAdapter = new MenuRecyclerViewAdapter(new ArrayList<PizzaModel>(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(menuRecyclerViewAdapter);

        menuViewModel = ViewModelProviders.of(this).get(MenuViewModel.class);
        menuViewModel.getPizzaList().observe(this, new Observer<List<PizzaModel>>() {
            @Override
            public void onChanged(@Nullable List<PizzaModel> pizzaModels) {
                menuRecyclerViewAdapter.updateList(pizzaModels);
            }
        });
        menuViewModel.getToastMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String message) {
                CustomSnackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
            }
        });
        menuViewModel.getCartCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                cartCount = integer;
                getActivity().invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(outState==null)
            super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.main_option_menu, menu);

        final View menu_icon = menu.findItem(R.id.menu_cart).getActionView();

        TextView counter = menu_icon.findViewById(R.id.actionbar_cart_count);
        counter.setText(String.valueOf(cartCount));

        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menu.findItem(R.id.menu_cart));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_cart:

                if (getFragmentManager().findFragmentByTag(CheckoutFragment.class.getSimpleName()) == null){
                    getFragmentManager().beginTransaction()
                            .add(R.id.main_container, new CheckoutFragment(), CheckoutFragment.class.getSimpleName())
                            .addToBackStack(CheckoutFragment.class.getSimpleName())
                            .commit();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClicked(PizzaModel pizzaModel) {
        menuViewModel.addPizzaToCart(pizzaModel);
    }
}
