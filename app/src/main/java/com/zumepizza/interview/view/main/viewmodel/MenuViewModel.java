package com.zumepizza.interview.view.main.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.zumepizza.interview.R;
import com.zumepizza.interview.data.AppDatabase;
import com.zumepizza.interview.data.model.CartItem;
import com.zumepizza.interview.data.model.PizzaModel;
import com.zumepizza.interview.network.API;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class MenuViewModel extends AndroidViewModel implements API.ResponseHandler{

    private AppDatabase appDatabase;
    private LiveData<List<PizzaModel>> pizzaList;
    private LiveData<Integer> cartCount;
    private API api;
    private MediatorLiveData<String> toastMessage;

    private Handler showMessageHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg != null && msg.obj != null)
                toastMessage.setValue(msg.obj.toString());
        }
    };

    public MenuViewModel(@NonNull Application application) {
        super(application);

        appDatabase = AppDatabase.getDatabase(this.getApplication());

        api = new API(this.getApplication());
        api.fetchMenu(this);

        pizzaList = appDatabase.pizzaModelDao().getAllPizzaModel();

        cartCount = appDatabase.cartItemDao().getCount();

        toastMessage = new MediatorLiveData<>();
    }

    public void addPizzaToCart(final PizzaModel pizzaModel){
        final CartItem cartItem = new CartItem();
        cartItem.setPizza(pizzaModel);
        cartItem.setCount(1);
        cartItem.setPizzaId(pizzaModel.getId());

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                Message msg = new Message();
                if (appDatabase.cartItemDao().addToCart(cartItem) > 0){
                    msg.obj = String.format(getApplication().getResources().getString(R.string.item_added_to_cart), pizzaModel.getName());
                } else {
                    msg.obj = getApplication().getResources().getString(R.string.item_add_cart_error);
                }
                showMessageHandler.sendMessage(msg);

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    public LiveData<List<PizzaModel>> getPizzaList(){
        return this.pizzaList;
    }

    public MediatorLiveData<String> getToastMessage(){
        return this.toastMessage;
    }

    public LiveData<Integer> getCartCount() {
        return this.cartCount;
    }

    @Override
    public void completion(final JSONArray response) {

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {

                if (response != null){
                    for (int i=0; i<response.length(); i++){
                        try {
                            PizzaModel pizzaModel = new Gson().fromJson(response.getJSONObject(i).toString(), PizzaModel.class);
                            appDatabase.pizzaModelDao().insert(pizzaModel);
                        }catch (JSONException e){
                            // TODO:: Handle exception
                        }
                    }
                } else {
                    // TODO:: Handle empty list
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();

    }

    @Override
    public void onFetchError(String message) {
        this.toastMessage.setValue(message);
    }
}
