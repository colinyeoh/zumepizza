package com.zumepizza.interview.view.checkout.fragment;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zumepizza.interview.R;
import com.zumepizza.interview.data.model.CartItem;
import com.zumepizza.interview.view.checkout.adapter.CheckoutRecyclerViewAdapter;
import com.zumepizza.interview.view.checkout.viewmodel.CheckoutViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ColinYeoh on 5/12/18.
 */

public class CheckoutFragment extends DialogFragment implements CheckoutRecyclerViewAdapter.OnIconClickedListener{

    private CheckoutRecyclerViewAdapter checkoutRecyclerViewAdapter;
    private RecyclerView recyclerView;
    private CheckoutViewModel viewModel;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.checkout_layout_main, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.checkout_recyclerView);

        checkoutRecyclerViewAdapter = new CheckoutRecyclerViewAdapter(new ArrayList<CartItem>(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(checkoutRecyclerViewAdapter);

        viewModel = ViewModelProviders.of(this).get(CheckoutViewModel.class);
        viewModel.getCartItems().observe(this, new Observer<List<CartItem>>() {
            @Override
            public void onChanged(@Nullable List<CartItem> cartItems) {
                checkoutRecyclerViewAdapter.update(cartItems);
            }
        });
    }

    @Override
    public void increment(int cartItemId) {
        viewModel.increment(cartItemId);
    }

    @Override
    public void decrement(int cartItemId) {
        viewModel.decrement(cartItemId);
    }

    @Override
    public void delete(CartItem cartItem) {
        viewModel.delete(cartItem);
    }
}
