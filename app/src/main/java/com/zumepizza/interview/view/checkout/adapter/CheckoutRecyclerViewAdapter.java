package com.zumepizza.interview.view.checkout.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.zumepizza.interview.R;
import com.zumepizza.interview.data.model.CartItem;

import java.util.List;

/**
 * Created by ColinYeoh on 5/12/18.
 */

public class CheckoutRecyclerViewAdapter
        extends RecyclerView.Adapter<CheckoutRecyclerViewAdapter.ViewHolder> {

    private List<CartItem> datas;
    private OnIconClickedListener listener;

    public interface OnIconClickedListener{
        void increment(int cartItemId);
        void decrement(int cartItemId);
        void delete(CartItem cartItem);
    }

    public CheckoutRecyclerViewAdapter(List<CartItem> datas, OnIconClickedListener listener){
        this.datas = datas;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkout_layout_cell, parent,false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CartItem item = this.datas.get(position);

        holder.pizzaName.setText(item.getPizza().getName());
        holder.pizzaImage.setImageURI(item.getPizza().getMenuAsset().getUrl());

        if (item.getPizza().getClassifications() != null
                && item.getPizza().getClassifications().getVegetarian() != null
                && item.getPizza().getClassifications().getVegetarian()){
            holder.iconVegetarian.setVisibility(View.VISIBLE);
        } else {
            holder.iconVegetarian.setVisibility(View.GONE);
        }

        holder.quantity.setText("x"+item.getCount());

        double price = Double.valueOf(item.getPizza().getPrice()) * item.getCount();
        holder.price.setText(String.format("$  %.2f",price));

        holder.iconAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.increment(item.getId());
            }
        });

        holder.iconMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.decrement(item.getId());
            }
        });

        holder.iconDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.delete(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.datas.size();
    }

    public void update(List<CartItem> cartItems) {
        this.datas = cartItems;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        SimpleDraweeView pizzaImage;
        TextView pizzaName;
        ImageView iconVegetarian;
        ImageView iconAdd;
        ImageView iconMinus;
        ImageView iconDelete;
        TextView quantity;
        TextView price;

        public ViewHolder(View itemView) {
            super(itemView);
            pizzaImage = itemView.findViewById(R.id.checkout_image);
            pizzaName = itemView.findViewById(R.id.checkout_pizza_name);
            iconVegetarian = itemView.findViewById(R.id.checkout_icon_vegetation);
            iconAdd = itemView.findViewById(R.id.checkout_add);
            iconMinus = itemView.findViewById(R.id.checkout_minus);
            iconDelete = itemView.findViewById(R.id.checkout_delete);
            quantity = itemView.findViewById(R.id.checkout_quantity);
            price = itemView.findViewById(R.id.checkout_price);
        }
    }
}
