package com.zumepizza.interview.view.main.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.zumepizza.interview.R;
import com.zumepizza.interview.data.model.PizzaModel;
import com.zumepizza.interview.data.model.Topping;

import java.util.List;


/**
 * Created by ColinYeoh on 5/11/18.
 */

public class MenuRecyclerViewAdapter extends RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder>{

    private List<PizzaModel> datas;
    private OnAddToCartClickListener onClickListener;

    public interface OnAddToCartClickListener {
        void onClicked(PizzaModel pizzaModel);
    }

    public MenuRecyclerViewAdapter(List<PizzaModel> datas, OnAddToCartClickListener listener){
        this.datas = datas;
        this.onClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_item_cell, parent,false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PizzaModel pizza = this.datas.get(position);

        holder.pizzaImage.setImageURI(Uri.parse(pizza.getMenuAsset().getUrl()));

        holder.pizzaName.setText(pizza.getName());

        if (pizza.getClassifications() != null
                && pizza.getClassifications().getVegetarian() != null
                && pizza.getClassifications().getVegetarian()){
            holder.iconVegetarian.setVisibility(View.VISIBLE);
        } else {
            holder.iconVegetarian.setVisibility(View.GONE);
        }

        // Setup toppings
        StringBuilder toppingBuilder = new StringBuilder();
        for (Topping topping : pizza.getToppings()){
            toppingBuilder.append(topping.getName()).append(", ");
        }
        // Remove the last ", "
        toppingBuilder.delete(toppingBuilder.length()-2, toppingBuilder.length()-1);
        holder.pizzaToppings.setText(toppingBuilder.toString());

        holder.pizzaPrice.setText(
                String.format(holder.itemView.getContext().getString(R.string.price_prefix), pizza.getPrice()));

        holder.iconAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClicked(pizza);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.datas.size();
    }

    public void updateList(List<PizzaModel> pizzaModels) {
        this.datas = pizzaModels;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView pizzaImage;
        ImageView iconAddToCart;
        TextView pizzaName;
        ImageView iconVegetarian;
        TextView pizzaToppings;
        TextView pizzaPrice;

        public ViewHolder(View itemView) {
            super(itemView);

            pizzaImage = itemView.findViewById(R.id.pizza_image);
            iconAddToCart = itemView.findViewById(R.id.icon_add_to_cart);
            pizzaName = itemView.findViewById(R.id.pizza_name_tv);
            iconVegetarian = itemView.findViewById(R.id.icon_vegetarian);
            pizzaToppings = itemView.findViewById(R.id.topping_tv);
            pizzaPrice = itemView.findViewById(R.id.price_tv);
        }
    }
}
