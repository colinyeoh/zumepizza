package com.zumepizza.interview;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;

/**
 * Created by ColinYeoh on 5/11/18.
 */

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize 3rd party libraries here if needed
        Fresco.initialize(this);

        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this);
    }
}
