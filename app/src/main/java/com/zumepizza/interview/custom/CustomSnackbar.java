package com.zumepizza.interview.custom;

import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zumepizza.interview.R;

/**
 * Created by ColinYeoh on 5/13/18.
 */

public class CustomSnackbar extends BaseTransientBottomBar<Snackbar> {

    /**
     * Constructor for the transient bottom bar.
     *
     * @param parent              The parent for this transient bottom bar.
     * @param content             The content view for this transient bottom bar.
     * @param contentViewCallback The content view callback for this transient bottom bar.
     */
    protected CustomSnackbar(@NonNull ViewGroup parent, @NonNull View content, @NonNull ContentViewCallback contentViewCallback) {
        super(parent, content, contentViewCallback);
    }

    public static Snackbar make(@NonNull View view, @NonNull String message, @NonNull int length){

        Snackbar snackbar = Snackbar.make(view, message, length);
        snackbar.getView().setBackgroundColor(view.getContext().getColor(R.color.orange));

        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(view.getContext().getColor(android.R.color.white));

        return snackbar;
    }
}
